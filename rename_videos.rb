require 'aws/s3'

AWS.config(
  region: 'us-east-1',
  access_key_id: ENV['AWS_ACCESS_KEY_ID'],
  secret_access_key: ENV['AWS_SECRET_ACCESS_KEY']
)

def run # main method it start all others
  videos.each do |e|
  	p e
    obj = bucket.objects[e]
    new_key = SecureRandom.hex
	obj.copy_to("#{new_key}.mp4")
  end
end

def videos # method get a list of files in the bucket by name of a bucket (Оставляем, так ак все равно необходим список видео)
  elements = []
  bucket.objects.each do |o|
    elements << o.key if o.key.end_with? '.mp4'
  end
  elements
end

def bucket_name
  'vloop-transcoder-test-videos'
end

def bucket
  s3 = AWS::S3.new
  s3.buckets[bucket_name]
end

if __FILE__ == $0
  run
end
